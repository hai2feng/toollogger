package loggertool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Logging implements Runnable {

	OutputStream ostream = null;
	int eventOrder = 0;

	Process m_logcatprocess;
	InputStreamReader m_logcat_inputreader;
	BufferedReader m_logcat_reader;

	public Logging() {
		Global.eventTrace = new ArrayList<Data>();

	}

	@Override
	public void run() {

		String stringbuffer = "";

		final String command = Global.adb + " logcat";
		final String clear = Global.adb + " logcat -c";

		try {

			Runtime.getRuntime().exec(clear);
			m_logcatprocess = Runtime.getRuntime().exec(command);
			m_logcat_inputreader = new InputStreamReader(m_logcatprocess.getInputStream());
			m_logcat_reader = new BufferedReader(m_logcat_inputreader);

			while ((stringbuffer = m_logcat_reader.readLine()) != null) {

				// System.out.println(stringbuffer);

				if (LogTool.jTextArea1 == null) {
					continue;
				}

				if (!Global.isConnectedToAdb) {
					Global.isConnectedToAdb = true;
					LogTool.jTextArea1.setText("Connected to adb\n");
				}

				// System.out.println("running " + Global.running);
				if (!Global.running) {
					continue;
				}

				// final long now = System.currentTimeMillis();

				final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
				final Date date = new Date();
				final String datestamp = dateFormat.format(date);

				// System.out.println("now " + now);
				if (isEvent(stringbuffer) || isSysCall(stringbuffer) || isParameter(stringbuffer)
						|| stringbuffer.indexOf("RPC_LOC_EVENT_SATELLITE_REPORT") != -1) {

					keeptrackof(datestamp, stringbuffer);
					LogTool.jTextArea1.append(stringbuffer + "\n");
				}

			}
		} catch (final IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void finalize() {
		m_logcatprocess.destroy();
	}

	private void keeptrackof(String datestamp, String s) {
		// System.out.println(now + ":" + s);
		Global.eventTrace.add(new Data(datestamp, s));

	}

	private boolean isActivityManger(String stringbuffer) {
		if (stringbuffer.contains("I/ActivityManager")) {
			return true;
		}
		return false;
	}

	private boolean isEvent(String stringbuffer) {

		if (stringbuffer.contains("EventHandler")) {
			return true;
		}
		return false;
	}

	private boolean isParameter(String stringbuffer) {

		if (stringbuffer.contains("ResourceParameters")) {
			return true;
		}
		return false;
	}

	private boolean isSysCall(String stringbuffer) {

		if (stringbuffer.contains("DroidBox")) {
			return true;
		}
		return false;
	}
}

package loggertool;

public class Data {
	public String timestamp;
	public String event;
	int camera;
	int flash;
	int gps;
	int wifi;
	int radio_3g;
	int screen;
	int cpu_freq;
	int cpu_load;

	public Data(String datestamp, String e) {
		this.timestamp = datestamp;
		this.event = e;
		camera = 0;
		flash = 0;
		gps = 0;
		wifi = 0;
		radio_3g = 0;
		screen = 0;
		cpu_freq = 0;
		cpu_load = 0;
	}

}
